declare var angular;

export class ContactUsController {
  model = {
    _fh: 'contact',
    recipient: '',
    name: '',
    email: '',
    subject: '',
    product: '',
    message: '',
  }
  endpoint: string = '';
  baseUrl: string = '';
  products: any[] = [];

  locks: string[] = [];  
  render: boolean = false;
  inProgress: boolean = false;
  success: boolean = false;

  error: string = '';

  subjects = [
    { 'id': 'buycoffee', 'title': 'I want to buy roasted coffee' },
    { 'id': 'booktour', 'title': 'I want to book a Coffee Tour' },
    { 'id': 'visitcoffeecenter', 'title': 'I want to buy green beans' },
    { 'id': 'educational', 'title': 'I want to subscribe to one of your educational programs' },
    { 'id': 'other', 'title': 'Other' },
  ];

  static $inject = ['$timeout', '$http'];
  constructor(
    private timeout,
    private http
  ) { }

  $postLink() {
    this.timeout(() => {
      this.render = true;
      this.parseLocationHash();
    }, 500);
  }

  selectSubject() {
    let id = this.getSubjectId(this.model.subject);

    if (id === 'booktour') {
      this.model.recipient = 'coffeecenter';
    } else {
      this.model.recipient = 'info';
    }
  }

  send(formEl) {
    if (formEl.$invalid) {
      return;
    }

    this.inProgress = true;

    this.http.post(this.endpoint, this.model)
      .then(response => {
        if (!response) {
          throw new Error('Internal server error');
        } else if (response.error) {
          throw new Error(response.error);
        }

        this.success = true;
        this.reset();
      })
      .catch(e => {
        if (!e) {
          this.error = 'Unknown error';
        } else if (typeof e === 'string') {
          this.error = e;
        } else if (e instanceof Error) {
          this.error = e.message;
        } else if (e.statusText) {
          this.error = `(${e.status}) ${e.statusText}`;
        } else {
          this.error = '?';
        }
      })
      .finally(() => {
        this.inProgress = false;
      });
  }

  reset() {
    this.locks = [];
    this.model._fh = 'contact';
    this.model.name = '';
    this.model.email = '';
    this.model.subject = '';
    this.model.product = '';
    this.model.message = '';
  }

  shouldShowProducts() {
    return this.model.subject == this.getSubjectLabel('buycoffee');
  }

  private parseLocationHash() {
    let hash = window.location.hash;

    if (!hash || hash.charAt(1) !== '?') {
      return;
    }

    let queryString = hash.substr(2),
      query = queryString.split(';'),
      data: any = {}, item;
    
    for (let i = 0; i < query.length; i++) {
      item = query[i].split('=');
      if (item.length === 1) {
        data[item[0]] = true;
      } else if (item.length === 2) {
        data[item[0]] = item[1];
      }
    }

    if (data.subject) {
      this.model.subject = this.getSubjectLabel(data.subject);
    }

    if (data.product) {
      this.model.product = this.getProductLabel(data.product);
    }
  }

  private getSubjectLabel(subjectId: string) {
    for (let i = 0; i < this.subjects.length; i++) {
      if (subjectId == this.subjects[i].id) {
        return this.subjects[i].title;
      }
    }

    return '';
  }

  private getSubjectId(subjectLabel: string) {
    for (let i = 0; i < this.subjects.length; i++) {
      if (subjectLabel == this.subjects[i].title) {
        return this.subjects[i].id;
      }
    }

    return '';
  }

  private getProductLabel(productId: string | number) {
    for (let i = 0; i < this.products.length; i++) {
      if (productId == this.products[i].id) {
        return this.products[i].title;
      }
    }

    return '';
  }
}
