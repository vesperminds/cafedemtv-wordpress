declare var $, Swiper;

export class SimpleHelper {
  mobilePaneBodyClass: string = 'mobile-pane--menu-opened';

  private body: HTMLElement;

  private careerSlider;

  constructor() {
    this.body = document.body;
  }

  bootstrap() {
    this.setupSpecialNav();
    this.setupMobilePaneToggle();
    this.setupHeroSlider();
  }

  setupSpecialNav() {
    let $body = $(this.body);

    $body.on('click', '.nav .ignore-click > a', e => {
      e.preventDefault();
    });
  }

  setupMobilePaneToggle() {
    let $body = $(this.body);

    $('#mobile-pane-toggle-button').on('click', e => {
      e.preventDefault();

      $body.toggleClass(this.mobilePaneBodyClass);
    });

    $('[mobile-pane-ignore-click], .mobile-pane-ignore-click').on('click', e => e.stopPropagation());

    $body.on('click', e => {
      if (!$body.hasClass(this.mobilePaneBodyClass)) {
        return;
      }

      $body.removeClass(this.mobilePaneBodyClass);
    });
  }

  setupHeroSlider() {
    if (!$('.page__hero-image .swiper-container').length) {
      return;
    }

    this.careerSlider = new Swiper('.page__hero-image .swiper-container', {
      loop: true,
      grabCursor: true,
      autoplay: 5000,
      slidesPerView: 1,
      pagination: '.swiper-pagination',
      paginationClickable: true,
      autoplayDisableOnInteraction: false,
    });
  }
}
