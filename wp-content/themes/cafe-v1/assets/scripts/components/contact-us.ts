import { ContactUsController } from '../controllers/contact-us';

export let ContactUsComponent = {
  templateUrl: 'components/contact-us.html',
  controller: ContactUsController,
  bindings: {
    endpoint: '@',
    baseUrl: '@',
    products: '<',
  }
};
