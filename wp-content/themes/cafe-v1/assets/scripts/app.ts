declare var angular, $;

import { ContactUsComponent } from './components/contact-us';

let templates = angular.module('templates', []),
  app = angular.module('ngAppCafe', ['templates']);

app.component('contactUs', ContactUsComponent);

if (!(<any>window)._DEBUG) {
  app.config(['$compileProvider', function ($compileProvider) {
    $compileProvider.debugInfoEnabled(false);
  }]);
}

// Helpers

import { SimpleHelper } from './helpers/simple';

$(() => (new SimpleHelper).bootstrap());
