<?php
    $products = get_all_products([
        'content' => true,
    ]);
?>
<?php get_template_part('parts/head') ?>

<?php wp_reset_postdata(); ?>
<div <?php post_class(['container', 'listable', 'products']) ?>>
    <?php get_template_part('parts/generic-hero') ?>
    <div class="row">
        <div class="gr-12 page__content">
            <div class="row">
                <div class="gr-12 page__content-cta">
                    <a class="ui-button ui-button--accent" href="<?= vp_url('/contact-us/#?subject=buycoffee') ?>">
                        For orders or more information click here
                    </a>
                </div>
            </div>

            <?php foreach($products as $product): ?>
            <div class="row row-align-middle listable__item">
                <div class="gr-4 gr-12@mobile">
                    <div class="listable__item-thumb"
                        <?= $product['thumb'] ? 'style="background-image: url(\'' . $product['thumb'] . '\')"' : '' ?>
                    ></div>
                </div>
                <div class="gr-8 gr-12@mobile listable__item-content">
                    <h3><?= $product['title'] ?></h3>
                    <p>
                        <?= $product['content'] ?>
                    </p>
                </div>
            </div>
            <?php endforeach; ?>

            <div class="row">
                <div class="gr-12 page__content-cta page__content-cta--bottom">
                    <a class="ui-button ui-button--accent" href="<?= vp_url('/contact-us/#?subject=buycoffee') ?>">
                        For orders or more information click here
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>

<?php get_template_part('parts/tail') ?>
