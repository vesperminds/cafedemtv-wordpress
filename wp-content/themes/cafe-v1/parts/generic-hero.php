<?php
    $meta = vp_fetch_meta(get_the_ID());
    $title = get_the_title();
    $date = '';

    if ($meta->has('custom-page-hero-text')) {
        $title = $meta->get('custom-page-hero-text');
    }

    if (get_post_type() === 'post') {
        $date = get_the_date();
    }

    // Images

    $images = [];

    $thumbnail_id = get_post_thumbnail_id(get_the_ID());

    if (!empty($thumbnail_id)) {
        $image = wp_get_attachment_image_src($thumbnail_id, 'vesper-hero-image');

        if (!empty($image)) {
            $images[] = $image[0];
        }
    }

    if (function_exists('get_images_src')) {
        $multi = get_images_src('vesper-hero-image', false, get_the_ID());
        
        if ($multi) {
            array_walk($multi, function($image) use (&$images) {
                $images[] = $image[0];
            });
        }
    }
?>
<div class="row row-full">
    <div class="gr-12 no-gutter page__hero-image <?= count($images) > 1 ? 'page__hero-slider' : '' ?>"
        <?php if (count($images) === 1): ?>
            style="background-image: url('<?= $images[0] ?>');"
        <?php endif; ?>
    >
        <?php if (count($images) > 1): ?>
        <div class="swiper-container">
            <div class="swiper-wrapper">
                <?php foreach ($images as $image): ?>
                <div
                    class="swiper-slide"
                    style="background-image: url('<?= $image ?>')"
                ></div>
                <?php endforeach; ?>
            </div>

            <div class="swiper-pagination"></div>
        </div>
        <?php endif; ?>

        <div class="page__hero-content">
            <h2 class="page__hero-title"><?= $title ?></h2>
            <?php if ($date): ?>
            <span class="page__hero-meta"><?= $date ?></span>
            <?php endif; ?>
        </div>
    </div>
</div>
