<!DOCTYPE html>
<html lang="es" ng-app="ngAppCafe" ng-strict-di>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, user-scalable=no">
    <title><?= th_title() ?></title>
    <link rel="stylesheet" href="https://use.fontawesome.com/88265fc2f5.css">
    <?php wp_head(); ?>
    <?php if (strpos($_SERVER['SERVER_NAME'], 'local.dev') !== false): ?>
    <script>window._DEBUG = 1;</script>
    <?php endif; ?>
</head>
<body>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/es_LA/sdk.js#xfbml=1&version=v2.7&appId=1193498367362566";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<?php if (
    strpos($_SERVER['SERVER_NAME'], 'local.dev') === false &&
    strpos($_SERVER['SERVER_NAME'], '.vesperminds.com') === false
): ?>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-84982421-1', 'auto');
  ga('send', 'pageview');

</script>
<?php endif; ?>
