<footer class="footer">
    <div class="container">
        <div class="row row-full row-align-middle">
            <div class="gr-12@mobile gr-grow@non-mobile footer__contact-info">
                <div class="gr-adapt-content">
                    (506) 2645-7550<br>
                    info@lifemonteverde.com
                </div>
            </div>
            <div class="gr-12@mobile gr-adapt@non-mobile footer__social-icons">
                <div class="gr-adapt-content">
                    <a class="footer__icons" href="https://www.facebook.com/cafedemonteverde/" target="_blank"><i class="fa fa-facebook"></i></a>
                    <a class="footer__icons" href="#"><i class="fa fa-twitter"></i></a>
                    <a class="footer__icons" href="https://www.instagram.com/cafedemonteverde/" target="_blank"><i class="fa fa-instagram"></i></a>
                    <a class="footer__icons" href="https://www.tripadvisor.com/g951347-d8655098" target="_blank"><i class="fa fa-tripadvisor"></i></a>
                </div>
            </div>
        </div>
    </div>
</footer>
