<div class="bottom-tiles container">
    <div class="row row-full">
        <div class="gr-3 gr-12@mobile bottom-tiles__cell bottom-tiles__main">
            <div class="bottom-tiles__wrapper">
                <h3>
                    Our coffee culture
                </h3>
                <p>
                    <a class="ui-button ui-button--accent" href="<?= vp_url('/tour/') ?>">Coffee Tour</a>
                </p>
            </div>
        </div>
        <div class="gr-3 gr-12@mobile bottom-tiles__cell bottom-tiles__life-monteverde">
            <div class="bottom-tiles__wrapper">
                <h3>
                    Sustainable agriculture
                </h3>
                <p>
                    <a class="ui-button" href="<?= vp_url('/life-monteverde/') ?>">Life Monteverde</a>
                </p>
            </div>
        </div>
        <div class="gr-3 gr-12@mobile bottom-tiles__cell bottom-tiles__coffee-center">
            <div class="bottom-tiles__wrapper">
                <h3>
                    Best cup in town
                </h3>
                <p>
                    <a class="ui-button" href="<?= vp_url('/coffee-center/') ?>">Coffee Center</a>
                </p>
            </div>
        </div>
        <div class="gr-3 gr-12@mobile bottom-tiles__cell bottom-tiles__products">
            <div class="bottom-tiles__wrapper">
                <h3>
                    Our coffee at home
                </h3>
                <p>
                    <a class="ui-button" href="<?= vp_url('/products/') ?>">Our Products</a>
                </p>
            </div>
        </div>
    </div>
</div>
