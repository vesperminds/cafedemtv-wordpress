<?php get_template_part('parts/head') ?>

<?php wp_reset_postdata(); ?>
<div <?php post_class(['container']) ?>>
    <?php get_template_part('parts/generic-hero') ?>
    <div class="row">
        <div class="gr-12 page__content page__generic-content">
            <?php the_content(); ?>
        </div>
    </div>
</div>

<?php get_template_part('parts/tail') ?>
