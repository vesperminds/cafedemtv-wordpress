<?php get_template_part('parts/head') ?>

<?php wp_reset_postdata(); ?>
<div <?php post_class(['container', 'listable', 'blog']) ?>>
    <div class="row row-full">
        <div class="gr-12 no-gutter page__hero-image blog__hero-image">
            <div class="page__hero-content">
                <h2 class="page__hero-title">Blog</h2>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="gr-12 page__content">
            <?php while(have_posts()): the_post() ?>
            <div class="row row-align-middle listable__item">
                <div class="gr-3 gr-12@mobile">
                    <div class="listable__item-thumb"
                        <?php if (has_post_thumbnail()): ?>
                            style="background-image: url('<?php the_post_thumbnail_url() ?>');"
                        <?php endif; ?>
                    ></div>
                </div>
                <div class="gr-6 gr-12@mobile listable__item-content">
                    <h3><?php the_title() ?></h3>
                    <p>
                        <?php the_excerpt() ?>
                    </p>
                </div>
                <div class="gr-3 gr-12@mobile listable__action">
                    <a class="ui-button ui-button--accent" href="<?php the_permalink() ?>">Read More</a>
                </div>
            </div>
            <?php endwhile; ?>
        </div>
    </div>
    <div class="row">
        <div class="gr-12 blog__pagination">
            <?php posts_nav_link(' ', '&laquo; Newer articles', 'Older articles &raquo;'); ?>
        </div>
    </div>
</div>

<?php get_template_part('parts/tail') ?>
