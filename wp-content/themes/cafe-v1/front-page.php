<?php get_template_part('parts/head', 'bare') ?>

<div class="container">
    <div class="row row-full nowrap wrap@mobile home__container">
        <div class="gr-8 gr-12@mobile no-gutter gr-grow@non-mobile">
            <?php get_template_part('sections/home', 'main') ?>
        </div>
        <div class="gr-4 gr-12@mobile no-gutter gr-grow@non-mobile">
            <?php get_template_part('sections/home', 'life-monteverde') ?>
            <?php get_template_part('sections/home', 'coffee-center') ?>
            <?php get_template_part('sections/home', 'products') ?>
        </div>
    </div>
</div>

<?php get_template_part('parts/tail', 'bare') ?>
