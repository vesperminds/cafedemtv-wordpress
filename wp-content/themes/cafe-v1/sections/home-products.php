<section class="home__products">
    <div class="home__section-wrapper">
        <h2>
            Enjoy our coffee at home
        </h2>
        <p>
            <a class="ui-button" href="<?= vp_url('/products/') ?>">Our Products</a>
        </p>
    </div>
</section>
