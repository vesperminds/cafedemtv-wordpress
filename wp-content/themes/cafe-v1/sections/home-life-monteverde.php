<section class="home__life-monteverde">
    <div class="home__section-wrapper">
        <h2>
            Learn about sustainable agriculture
        </h2>
        <p>
            <a class="ui-button" href="<?= vp_url('/life-monteverde/') ?>">Life Monteverde</a>
        </p>
    </div>
    <span class="life-monteverde-logo"></span>
</section>
