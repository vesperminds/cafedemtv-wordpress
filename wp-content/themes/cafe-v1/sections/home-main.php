<section class="home__main">
    <?php wp_nav_menu(['theme_location' => 'front-page-nav', 'container' => 'nav', 'container_class' => 'home__nav']) ?>

    <div class="home__section-wrapper">
        <h2>
            Embrace our coffee culture
        </h2>
        <p>
            <a class="ui-button ui-button--accent" href="<?= vp_url('/tour') ?>">Coffee Tour</a>
        </p>
    </div>
</section>
