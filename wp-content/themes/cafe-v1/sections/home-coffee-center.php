<section class="home__coffee-center">
    <div class="home__section-wrapper">
        <h2>
            Indulge yourself with the best cup in town
        </h2>
        <p>
            <a class="ui-button" href="<?= vp_url('/coffee-center/') ?>">Coffee Center</a>
        </p>
    </div>
</section>
