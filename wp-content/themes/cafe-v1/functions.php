<?php defined('ABSPATH') or die;

define('DEBUG_MODE', strpos($_SERVER['SERVER_NAME'], 'local.dev') !== false);

add_action('wp_enqueue_scripts', function() {
    $bowerDir = '/../../../bower_components';

    // CSS

    $sheets = [
        [ 'main', '/dist/css/main.css' ],
        [ 'swiper-css', $bowerDir . '/Swiper/dist/css/swiper.min.css' ],
    ];

    foreach ($sheets as $sheet) {
        wp_register_style(
            $sheet[0],
            vpth_path($sheet[1]),
            null,
            vpth_version($sheet[1]),
            'screen'
        );
        wp_enqueue_style($sheet[0]);
    }

    // JS

    $scripts = [
        [ 'jquery', $bowerDir . '/jquery/dist/jquery.min.js' ],
        [ 'angular', $bowerDir . '/angular/angular.min.js' ],
        [ 'swiper', $bowerDir . '/Swiper/dist/js/swiper.jquery.min.js' ],
        [ 'app', '/dist/js/scripts/app.js' ],
        [ 'templates', '/dist/js/templates/templates.js' ],
    ];

    foreach ($scripts as $script) {
        $ext = isset($script[2]) && $script[2];

        wp_deregister_script($script[0]);
        wp_register_script(
            $script[0],
            $ext ? $script[1] : vpth_path($script[1]),
            null,
            $ext ? md5($script[1]) : vpth_version($script[1]),
            true
        );
    }

    foreach($scripts as $script) {
        wp_enqueue_script($script[0]);
    }
});

/* Title */

$_overrideTitle = null;

function th_title_suffix() {
    return ' &mdash; Café de Monteverde';
}

function th_set_page_title($title = 'Café de Monteverde') {
    global $_overrideTitle;

    if (empty($title)) {
        return;
    }

    $_overrideTitle = $title;
}

function th_title() {
    return wp_title(th_title_suffix(), true, 'right');
}

add_filter('wp_title', function($title) {
    global $_overrideTitle;

    if (!empty($_overrideTitle)) {
        return $_overrideTitle . th_title_suffix();
    }

    if (empty($title) && (is_home() || is_front_page())) {
        return 'Café de Monteverde';
    }

    return $title;
});

/* Images */

add_theme_support('post-thumbnails', [
    'post',
    'page',
    'product',
]);

add_action('init', function() {
    add_image_size('vesper-hero-image', 1200, 740);
    add_image_size('vesper-mobile-hero-image', 960, 592);
    add_image_size('vesper-post-thumbnail', 600, 600, true);
    add_image_size('vesper-post-thumbnail-cover', 600, 600);
});

/* JSON */

function tt_json_attr($data = null) {
    return htmlspecialchars(json_encode($data), ENT_QUOTES);
}

function tt_json_output($data) {
    @header('Content-Type: application/json; charset=utf-8');
    echo json_encode($data);
}

/* Menus */

add_action('init', function() {
    register_nav_menus([
        'main-nav' => 'Navegación Estandar',
        'front-page-nav' => 'Navegación Reducida en Inicio'
    ]);
});

/* Shortcodes */

add_shortcode('button', function($atts, $content = '') {
    $atts = shortcode_atts([
        'href' => '#'
    ], $atts, 'button');

    if ($atts['href'] && substr($atts['href'], 0, 1) === '/') {
        $atts['href'] = vp_url($atts['href']);
    }

    return '<a class="ui-button ui-button--accent ui-button--big" href="' . $atts['href'] . '">' . $content . '</a>';
});

add_shortcode('gr_row', function($atts, $content = '') {
    $atts = shortcode_atts([
        'class' => ''
    ], $atts, 'gr_row');

    return '<div class="row ' . $atts['class'] . '">' . do_shortcode($content) . '</div>';
});

add_shortcode('gr_col', function($atts, $content = '') {
    $atts = shortcode_atts([
        'n' => 12,
        'nm' => 12,
        'class' => '',
    ], $atts, 'gr_row');

    return '<div class="' . "gr-{$atts['n']} gr-{$atts['nm']}@mobile {$atts['class']}" . '">' . do_shortcode($content) . '</div>';
});

/* Posts */

add_filter('excerpt_length', function() { return 32; }, 995);

/* Form */

define('EMAIL_EOL', "\r\n");

$tt_form_handlers = [

    'contact' => function($step, $data) {

        if ($step == 'validate') {

            $errors = [];

            if (empty($data['subject'])) {
                $errors[] = 'subject';
            }

            if (empty($data['name'])) {
                $errors[] = 'name';
            }

            if (empty($data['email']) || !filter_var($data['email'], FILTER_VALIDATE_EMAIL)) {
                $errors[] = 'email';
            }

            if (empty($data['message'])) {
                $errors[] = 'message';
            }

            return $errors;

        }
        elseif ($step == 'compose') {

            $message = [];

            $message[] = 'Contact Form';
            $message[] = '------------';
            $message[] = '';
            $message[] = 'Name: ' . strip_tags($data['name']);
            $message[] = 'Email: ' . strip_tags($data['email']);
            $message[] = 'Subject: ' . strip_tags($data['subject']);

            if (!empty($data['product'])) {
                $message[] = 'Product: ' . strip_tags($data['product']);
            }

            $message[] = 'Message:';
            $message[] = strip_tags($data['message']);

            return $message;

        }
        elseif ($step == 'subject') {
            return 'Contact Form - ' . strip_tags($data['subject']);
        }
        elseif ($step == 'compose-user') {

            $message = [];

            $message[] = 'Hey ' . $data['name'] . ',';
            $message[] = '';
            $message[] = 'Thank you for getting in touch with us. We\'ll reply you as soon as possible.';
            $message[] = '';
            $message[] = 'Thanks,';
            $message[] = '- Café de Monteverde team';


            return $message;

        }
        elseif ($step == 'subject-user') {
            return 'Thank you for contacting us';
        }
        elseif ($step == 'recipients') {
            $admin_rcpt = [];

            if ($data['recipient'] == 'coffeecenter') {
                $admin_rcpt[] = 'coffeecenter@lifemonteverde.com';
            } else {
                $admin_rcpt[] = 'info@lifemonteverde.com';
            }

            $admin_rcpt[] = 'vesperminds@gmail.com';

            return array_unique($admin_rcpt);
        }

        return false;

    },
];

function tt_handle_form() {
    global $tt_form_handlers;

    if ($_SERVER['REQUEST_METHOD'] != 'POST') {
        return;
    }

    if (empty($_POST)) {
        $_POST = json_decode(file_get_contents('php://input'), true);
    }

    if (empty($_POST['_fh']) || empty($tt_form_handlers[$_POST['_fh']])) {
        return;
    }

    $fh = $_POST['_fh'];

    $data = $_POST;

    foreach ($data as $key => $value) {
        $data[$key] = trim(strip_tags($value));
    }

    $validationErrors = $tt_form_handlers[$fh]('validate', $data);

    if (!empty($validationErrors)) {
        tt_json_output([
            'done' => false,
            'error' => 'E_INVALID_FIELDS',
            'message' => 'One or more fields are not valid',
            'details' => $validationErrors
        ]);
        exit;
    }

    $admin_rcpt = $tt_form_handlers[$fh]('recipients', $data);
    $subject = $tt_form_handlers[$fh]('subject', $data);
    $message = $tt_form_handlers[$fh]('compose', $data);

    if (!DEBUG_MODE) {
        wp_mail($admin_rcpt, $subject, implode(EMAIL_EOL, $message));
    } else {
        var_dump([$admin_rcpt, $subject, implode(EMAIL_EOL, $message)]);
    }

    $subjectUser = $tt_form_handlers[$fh]('subject-user', $data);
    $messageUser = $tt_form_handlers[$fh]('compose-user', $data);

    if (!DEBUG_MODE) {
        wp_mail($data['email'], $subjectUser, implode(EMAIL_EOL, $messageUser));
    } else {
        var_dump([$data['email'], $subjectUser, implode(EMAIL_EOL, $messageUser)]);
    }

    tt_json_output([
        'done' => true
    ]);
    exit;
}

