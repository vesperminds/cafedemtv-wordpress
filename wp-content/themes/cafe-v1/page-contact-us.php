<?php
    tt_handle_form();

    $products = get_all_products([ 'angular' => true ]);
?>
<?php get_template_part('parts/head') ?>

<?php wp_reset_postdata(); ?>
<div <?php post_class(['container', 'contact-us']) ?>>
    <?php get_template_part('parts/generic-hero') ?>
    <div class="row">
        <div class="gr-12 contact-us__form">
            <p class="contact-us__form-heading">
                To take our tour or buy some coffee, please get in touch us:
            </p>
            <contact-us
                endpoint="<?= the_permalink() ?>"
                base-url="<?= vp_url() ?>"
                products="<?= tt_json_attr($products) ?>"
            ></contact-us>
        </div>
        <div class="gr-12 page__content">
            <div class="row">
                <div class="gr-5 suffix-1 gr-12@mobile suffix-0@mobile contact-us__item">
                    <h2 class="contact-us__title"> Monteverde Coffee Tour &amp; Life Monteverde Farm</h2>
                    <ul class="contact-us__content">
                        <li>
                            <b>Phone</b>: <a href="tel:+50626457550">(506) 2645-7550</a>
                        </li>
                        <li>
                            <b>Email</b>: info@lifemonteverde.com
                        </li>
                        <li>
                            <b>Directions</b>: 400m South from Cañitas Church
                        </li>
                        <li>
                            <b>Schedule</b>: OPEN EVERYDAY from 7:30am to 4:30pm
                        </li>
                    </ul>
                    <p class="contact-us__content contact-us__icons">
                        <a target="_blank" href="https://www.google.co.cr/maps/place/Life+Monteverde/@10.3248518,-84.8447621,17z/data=!3m1!4b1!4m5!3m4!1s0x8fa01bee9aafab03:0x36f7248d9c22fb68!8m2!3d10.3248518!4d-84.8432145" class="contact-us__gmaps-link"></a>
                        <a href="https://www.waze.com/es/livemap?zoom=17&lat=10.32485&lon=-84.84321" class="contact-us__waze-link"></a>
                    </p>
                    <div class="contact-us__fb-plugin">
                        <div class="fb-page" data-href="https://www.facebook.com/lifemonteverde/" data-tabs="timeline" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/lifemonteverde/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/lifemonteverde/">Café de Monteverde</a></blockquote></div>
                    </div>
                </div>
                <div class="gr-5 prefix-1 gr-12@mobile prefix-0@mobile contact-us__item">
                    <h2 class="contact-us__title">Monteverde Coffee Center</h2>
                    <ul class="contact-us__content">
                        <li>
                            <b>Phone</b>: <a href="tel:+50626457550">(506) 2645-7550</a>
                        </li>
                        <li>
                            <b>Email</b>: coffeecenter@cafedemonteverde.com
                        </li>
                        <li>
                            <b>Directions</b>: Next to CASEM &ndash; Monteverde Community
                        </li>
                        <li>
                            <b>Schedule</b>: OPEN EVERYDAY from 7:00am to 6:00pm
                        </li>
                    </ul>
                    <p class="contact-us__content contact-us__icons">
                        <a target="_blank" href="https://www.google.co.cr/maps/place/Coffee+Center/@10.3067766,-84.812259,20z/data=!4m8!1m2!2m1!1sCafé+de+Monteverde!3m4!1s0x8fa019808ccb38af:0xa49be5112004e49e!8m2!3d10.3069076!4d-84.8120472" class="contact-us__gmaps-link"></a>
                        <a href="https://www.waze.com/es/livemap?zoom=17&lat=10.30691&lon=-84.81205" class="contact-us__waze-link"></a>
                    </p>
                    <div class="contact-us__fb-plugin">
                        <div class="fb-page" data-href="https://www.facebook.com/cafedemonteverde/" data-tabs="timeline" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/cafedemonteverde/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/cafedemonteverde/">Café de Monteverde</a></blockquote></div>
                    </div>
                </div>
            </div>
            <p class="contact-us__blog-link">
                <a href="<?= vp_url('/blog/') ?>" class="ui-button ui-button--accent ui-button--big">
                    Visit our blog
                </a>
            </p>
        </div>
    </div>
</div>

<?php get_template_part('parts/tail') ?>
