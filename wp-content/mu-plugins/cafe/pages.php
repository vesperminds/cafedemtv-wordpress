<?php defined('ABSPATH') or die;

add_action('init', function() {

    /* Type Meta */

    VP_MetaBox::registerForCustomPostType('page', [
        'id' => 'page-customization',
        'title' => 'Personalización de la página',
        'context' => 'normal',
        'priority' => 'default',

        'group-options' => [ ],

        'fields' => [

            'custom-page-hero-text' => [
                'label' => 'Texto en Banner',
                'type' => 'text',
                'maxlength' => 140,
            ]

        ]
    ]);

});
