<?php defined('ABSPATH') or die;

add_action('init', function() {

    /* Type Meta */

    VP_MetaBox::registerForCustomPostType('post', [
        'id' => 'page-customization',
        'title' => 'Personalización de la entrada',
        'context' => 'normal',
        'priority' => 'default',

        'group-options' => [ ],

        'fields' => [

            'custom-page-hero-text' => [
                'label' => 'Texto en Banner',
                'type' => 'text',
                'maxlength' => 140,
            ]

        ]
    ]);

});
