<?php defined('ABSPATH') or die;

add_action('init', function() {

    /* Type */

    register_post_type('product', [
        'labels' => [
            'name' => 'Productos',
            'singular_name' => 'Producto',
            'menu_name' => 'Productos',
            'name_admin_bar' => 'Producto',
            'add_new' => 'Agregar nuevo',
            'add_new_item' => 'Agregar nuevo producto',
            'new_item' => 'Nuevo producto',
            'edit_item' => 'Editar producto',
            'view_item' => 'Ver producto',
            'all_items' => 'Todos los productos',
            'search_items' => 'Buscar productos',
            'parent_item_color' => 'Producto padre:',
            'not_found' => 'No se encontraron productos.',
            'not_found_in_trash' => 'No se encontraron productos en la papelera.',
        ],
        'public' => true,
        'menu_position' => 20,
        'menu_icon' => 'dashicons-exerpt-view',
        'capability_type' => 'page',
        'supports' => [
            'title',
            'editor',
            'thumbnail',
            'page-attributes'
        ],
        'rewrite' => [
            'slug' => 'product'
        ]
    ]);

    /* Type Meta */

    VP_MetaBox::registerForCustomPostType('product', [
        'id' => 'product-information',
        'title' => 'Información del Producto',
        'context' => 'normal',
        'priority' => 'default',

        'group-options' => [ ],

        'fields' => [

            'product-id' => [
                'label' => 'ID de Producto',
                'type' => 'text',
                'maxlength' => 60,
            ]

        ]
    ]);

});

function get_all_products(array $opts = [], array $meta = []) {

    $opts = array_merge([
        'type' => 'product',
        'order' => 'ASC',
        'orderby' => 'menu_order',
        'thumbsize' => 'vesper-post-thumbnail-cover',
    ], $opts);

    $all = vp_get_all($opts);

    if (function_exists('get_images_src')) {
        array_walk($all, function(&$product) {
            $product['images'] = get_images_src('vesper-gallery-image', false, $product['id']);
        });
    }

    return $all;
}
