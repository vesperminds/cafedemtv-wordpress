<?php defined('ABSPATH') or die;
/**
 * Plugin Name: Café de Monteverde
 * Description: Non-theme settings for Café de Monteverde
 * Author: Vesper Minds
 * Author URI: http://vesperminds.com/
 * Network: true
 */

require_once __DIR__ . '/cafe/posts.php';
require_once __DIR__ . '/cafe/pages.php';
require_once __DIR__ . '/cafe/products.php';

/** changing default wordpres email settings */

add_filter('wp_mail_from', function($old = '') {
	return 'no-reply@cafedemonteverde.com';
});

add_filter('wp_mail_from_name', function($old = '') {
	return 'Café de Monteverde';
});

/* Multi images */

add_filter('images_cpt', function() {
    return ['page', 'post', 'product'];
});

add_filter('list_images', function($list_images, $cpt) {
    global $typenow;

    $qty = 10;

    if ($typenow == 'product' || $cpt == 'product') {
        $qty = 20;
    }

    $picts = [];

    for ($i = 1; $i <= $qty; $i++) {
        $picts["image{$i}"] = "_image{$i}";
    }

    return $picts;
}, 10, 2);

/**
 * Wordpress footer
 */
add_filter('admin_footer_text', function($text) {
	return $text . ' <i>Potenciado por <a href="http://vesperminds.com/" target="_blank">Vesper Minds</a><i>';
});
