<?php
/**
 * The Content Index plugin
 *
 * @link              http://www.easantos.net/wordpress/the-content-index/
 * @since             1.0.0
 * @package           The_Content_Index
 *
 * @wordpress-plugin
 * Plugin Name:       The Content Index
 * Plugin__NOT_USED__ URI:        http://www.easantos.net/wordpress/the-content-index/
 * Description:       The Content Index wordpress plugin will generate a content index for your posts and pages.
 * Version:           1.0.5
 * Author:            Easantos
 * Author URI:        http://www.easantos.net
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       the-content-index
 * Domain Path:       /languages
 */

if (!defined('ABSPATH')) exit; // Exit if accessed directly

add_action('the_content', 'tciContent', -1);
add_action('admin_menu', 'tciAddMenuItems');
register_activation_hook(__FILE__, 'tciSetDefaultSettings');
add_filter('plugin_action_links', 'tciPluginActionLinks', 10, 2);

require_once 'classes/TheContentIndex.php';


/** @var TheContentIndex $theContentIndex */
global $theContentIndex;

$theContentIndex = new TheContentIndex();

function tciPluginActionLinks($links, $file) {
    static $this_plugin;

    if (!$this_plugin) {
        $this_plugin = plugin_basename(__FILE__);
    }

    if ($file == $this_plugin) {
        $settings_link = '<a href="' . get_bloginfo('wpurl') . '/wp-admin/admin.php?page=tciSettings">Settings</a>';
        array_unshift($links, $settings_link);
    }

    return $links;
}

function tciAddMenuItems()
{
    /** @var TheContentIndex $theContentIndex */
    global $theContentIndex;

    $theContentIndex->addMenuItems();
}

function tciMain()
{
    /** @var TheContentIndex $theContentIndex */
    global $theContentIndex;

    $theContentIndex->main();
}

function tciSettings()
{
    /** @var TheContentIndex $theContentIndex */
    global $theContentIndex;

    $theContentIndex->settings();
}

function tciContent($content)
{
    /** @var TheContentIndex $theContentIndex */
    global $theContentIndex;

    return $theContentIndex->content($content);
}

function tciSetDefaultSettings()
{
    /** @var TheContentIndex $theContentIndex */
    global $theContentIndex;

    $theContentIndex->setDefaultSettings();
}
