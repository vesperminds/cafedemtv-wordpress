=== The Content Index ===
Contributors: easantos
Donate link: http://www.easantos.net/
Tags: content index, index, map of content, table of content, toc
Requires at least: 1.0.0
Tested up to: 4.5.2
Stable tag: 1.0.5
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Content Index shows your post index.

== Description ==

The Content Index wordpress plugin will generate a content index for your posts and pages.

= Credits =

This plugin was created by [Emanuel Santos](http://www.easantos.net) with with sponsorship from [Em Forma](http://www.emforma.net).

== Installation ==

1. Upload `the-content-index` directory to the `/wp-content/plugins/` directory
1. Activate the plugin through the 'Plugins' menu in WordPress

== Screenshots ==

1. Content Index basic example

== Changelog ==

= 1.0.5 =
* Added support for nested header elements

= 1.0.4 =
* Restricted functionality to post and page contents.

= 1.0.3 =
* More cleanup.

= 1.0.2 =
* More cleanup.

= 1.0.1 =
* Code cleanup.

= 1.0.0 =
* First version of the plugin.

== Upgrade Notice ==

= 1.0.5 =
* Added support for nested header elements
