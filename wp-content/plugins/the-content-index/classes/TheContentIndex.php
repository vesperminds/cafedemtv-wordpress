<?php
if (!defined('ABSPATH')) exit;

class TheContentIndex
{
    const PLUGIN_NAME = 'The Content Index';
    const PLUGIN_SLUG = 'the-content-index';
    const PLUGIN_DIRECTORY = '/the-content-index';
    const NOUNCE_EXPIRATION_TIME = 3600;

    private $allowedPostTypes = array('post', 'page', 'ef_article');

    /**
     * @param mixed $formNounce
     */
    public function setFormNounce($formNounce)
    {
        $this->formNounce = $formNounce;
    }

    private function getOurPlugins()
    {
        return array(
            array(
                'slug' => 'the-content-index',
                'name' => 'The Content Index',
            ),
            array(
                'slug' => 'import-social-statistics',
                'name' => 'Import Social Statistics',
            )
        );
    }

    public function addMenuItems()
    {
        add_menu_page('The Content Index', 'The Content Index', 10, 'tciMain', 'tciMain', plugins_url('images/icon.png', dirname(__FILE__)));
        add_submenu_page('tciMain', 'Settings', 'Settings', 1, 'tciSettings', 'tciSettings');
    }

    public function main()
    {
        require_once WP_PLUGIN_DIR . self::PLUGIN_DIRECTORY . '/views/main.php';
    }

    public function getFormNounceId()
    {
        return self::PLUGIN_SLUG . '-nounce';
    }

    public function validatePostBoolean($fieldName)
    {
        return !isset($_POST[$fieldName]) || $_POST[$fieldName] == '1';
    }

    public function validatePostArrayOfIds($fieldName)
    {
        return preg_match('/^[,0-9]*$/i', $_POST[$fieldName]) === 1;
    }

    public function validatePostString($fieldName)
    {
        return is_string($_POST[$fieldName]);
    }

    public function validatePostSettings()
    {
        $errors = array();

        if (!$this->validatePostBoolean('tciShowOnPosts')) {
            $errors[] = 'tciShowOnPosts field must be boolean.';
        }

        if (!$this->validatePostBoolean('tciShowOnPages')) {
            $errors[] = 'tciShowOnPages field must be boolean.';
        }

        if (!$this->validatePostArrayOfIds('tciHideOnIds')) {
            $errors[] = 'tciHideOnIds field must array of integer separated by a comma.';
        }

        if (!$this->validatePostString('tciContentIndexString')) {
            $errors[] = 'tciContentIndexString field must be a generic string.';
        }

        return $errors;
    }

    public function settings()
    {
        $tciOptions = get_option('tciOptions');

        // Check actions
        if (isset($_POST['action']) && get_transient($this->getFormNounceId()) !== FALSE && check_admin_referer(get_transient($this->getFormNounceId()))) {
            switch ($_POST['action']) {
                case 'save-settings':
                    if (count($this->validatePostSettings())) {
                        $errorMessages = $this->validatePostSettings();

                        break;
                    }

                    $tciOptions = [];
                    $tciOptions['tciShowOnPosts'] = intval($_POST['tciShowOnPosts']);
                    $tciOptions['tciShowOnPages'] = intval($_POST['tciShowOnPages']);
                    $tciOptions['tciHideOnIds'] = sanitize_text_field($_POST['tciHideOnIds']);
                    $tciOptions['tciContentIndexString'] = sanitize_text_field($_POST['tciContentIndexString']);

                    update_option(
                        'tciOptions',
                        $tciOptions
                    );

                    $successMessage = 'Settings saved. ';

                    break;
            }
        }

        set_transient(
            self::PLUGIN_SLUG . '-nounce',
            time(),
            self::NOUNCE_EXPIRATION_TIME
        );

        require_once WP_PLUGIN_DIR . self::PLUGIN_DIRECTORY . '/views/settings.php';
    }

    public function content($content)
    {
        if (!in_array(get_post_type(), $this->allowedPostTypes)) {
            return $content;
        }

        $tciOptions = get_option('tciOptions');

        if (!$tciOptions['tciShowOnPosts'] && is_single()) {
            return $content;
        }

        if (!$tciOptions['tciShowOnPages'] && is_page()) {
            return $content;
        }

        $tciHideOnIds = explode(',', $tciOptions['tciHideOnIds']);

        if (in_array(get_the_ID(), $tciHideOnIds)) {
            return $content;
        }

        $dom = new \DOMDocument();
        $internalErrors = libxml_use_internal_errors(TRUE);
        $dom->loadHTML($content);
        libxml_use_internal_errors($internalErrors);
        $xp = new DOMXPath($dom);

        $expression = '
(
    //h1
    |//h2
    |//h3
    |//h4
    |//h5
    |//h6
)';

        /** @var DOMNodeList $elements */
        $elements = $xp->query($expression);
        $htmls = [];

        if (count($elements) == 0 || $elements->length < 2) {
            return $content;
        }

        /** @var boolean $parentElement */
        $ulOpen = false;

        /** @var DOMElement $lastElement */
        $lastElement = null;

        /** @var DOMElement $element */
        foreach ($elements as $element) {
            if ($lastElement !== null && $lastElement->tagName != $element->tagName) {
                if ($ulOpen === true) {
                    $htmls [] = '</ul>';
                    $ulOpen = false;
                } else if ($ulOpen === false) {
                    $htmls [] = '<ul>';
                    $ulOpen = true;
                }
            }

            $element->nodeValue = htmlspecialchars(utf8_decode($element->nodeValue));

            $htmls [] = '<li><a href="#' . $this->basename($element->nodeValue) . '">' . $element->nodeValue . '</a></li>';

            $content = str_replace(
                [
                    '<' . $element->tagName . '>' . $element->nodeValue . '</' . $element->tagName . '>',
                    '<' . $element->tagName . '>' . htmlspecialchars(utf8_decode($element->nodeValue)) . '</' . $element->tagName . '>'
                ],
                '<' . $element->tagName . ' id="' . $this->basename($element->nodeValue) . '">' . $element->nodeValue . '</' . $element->tagName . '>',
                $content,
                $count
            );

            $lastElement = $element;
        }

        $htmlTable = '<table class="the-content-index"><tbody><tr class="tci-title"><td><strong>' . $tciOptions['tciContentIndexString'] . '</strong></td></tr><tr class="tci-items"><td><ul>' . implode('', $htmls) . '</ul></td></td></tr></tbody></table>';

        $content = str_replace(
            '<' . $elements[0]->tagName . ' id="' . $this->basename($elements[0]->nodeValue) . '">' . $elements[0]->nodeValue . '</' . $elements[0]->tagName . '>',
            $htmlTable . '<' . $elements[0]->tagName . ' id="' . $this->basename($elements[0]->nodeValue) . '">' . $elements[0]->nodeValue . '</' . $elements[0]->tagName . '>',
            $content
        );

        return $content;
    }

    public function insertContentIndexInContent($to, $subject, $firstH2Id)
    {
        $pattern = '/<h2 id="' . $this->basename($firstH2Id) . '">(.*?)<\/h2>/';

        return preg_replace(
            $pattern,
            $to,
            $subject,
            1
        );
    }

    public function basename($string, $separator = '-')
    {
        $accents_regex = '~&([a-z]{1,2})(?:acute|cedil|circ|grave|lig|orn|ring|slash|th|tilde|uml);~i';
        $special_cases = array('&' => 'and', "'" => '');
        $string = mb_strtolower(trim($string), 'UTF-8');
        $string = str_replace(array_keys($special_cases), array_values($special_cases), $string);
        $string = preg_replace($accents_regex, '$1', htmlentities($string, ENT_QUOTES, 'UTF-8'));
        $string = preg_replace("/[^a-z0-9]/u", "$separator", $string);
        $string = preg_replace("/[$separator]+/u", "$separator", $string);

        return $string;
    }

    public function setDefaultSettings()
    {
        $tciOptions = get_option('tciOptions', [
            'tciShowOnPosts' => 1,
            'tciShowOnPages' => 1,
            'tciHideOnIds' => '',
            'tciContentIndexString' => 'Content Index',
        ]);

        update_option('tciOptions', $tciOptions);
    }
}
